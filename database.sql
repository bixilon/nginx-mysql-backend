-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 27, 2021 at 09:47 PM
-- Server version: 10.5.8-MariaDB-1:10.5.8+maria~focal
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proxy`
--

-- --------------------------------------------------------

--
-- Table structure for table `proxy`
--

CREATE TABLE `proxy`
(
    `id`              int(11)                                            NOT NULL,
    `server_address`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`server_address`)),
    `ssl_certificate` varchar(512)                                       NOT NULL,
    `proxy_pass`      longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`proxy_pass`)),
    `status`          enum ('ENABLED','DISABLED')                        NOT NULL,
    `nodes`           longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`nodes`)),
    `config`          longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`config`))
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

--
-- Dumping data for table `proxy`
--

INSERT INTO `proxy` (`id`, `server_address`, `ssl_certificate`, `proxy_pass`, `status`, `nodes`, `config`)
VALUES (1, '[\"bixilon.de\", \"test.bixilon.de\"]', 'bixilon.de', '[\"192.168.1.2\", \"192.168.1.3\"]', 'ENABLED', '[\"192.168.1.1\", \"hostname\", \"moritz-tp\"]', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `vhost`
--

CREATE TABLE `vhost`
(
    `id`              int(11)                                            NOT NULL,
    `domain`          varchar(512)                                       NOT NULL,
    `server_address`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`server_address`)),
    `ssl_certificate` varchar(512)                                       NOT NULL,
    `username`        varchar(512)                                       NOT NULL,
    `script`          enum ('NONE','PHP7')                               NOT NULL,
    `status`          enum ('ENABLED','DISABLED')                        NOT NULL,
    `nodes`           longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`nodes`)),
    `config`          longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`config`))
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

--
-- Dumping data for table `vhost`
--

INSERT INTO `vhost` (`id`, `domain`, `server_address`, `ssl_certificate`, `username`, `script`, `status`, `nodes`, `config`)
VALUES (1, 'bixilon.de', '[\"*.bixilon.de\"]', 'bixilon.de', 'bixilon', 'NONE', 'ENABLED', '[\"moritz-tp\"]', '{}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `proxy`
--
ALTER TABLE `proxy`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vhost`
--
ALTER TABLE `vhost`
    ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `proxy`
--
ALTER TABLE `proxy`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 2;

--
-- AUTO_INCREMENT for table `vhost`
--
ALTER TABLE `vhost`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
