import json
import sys


def update(first, second):
    if type(first) == dict:
        for key in second:
            if key not in first:
                first[key] = second[key]
                continue
            if type(first[key]) != dict:
                first[key] = second[key]
                continue
            update(first[key], second[key])
        return first


def loadConfig():
    templateConfig = json.load(open("templates/config_template.json"))

    config = json.load(open(sys.argv[1]))

    # merge 2 dicts
    update(templateConfig, config)

    return templateConfig
