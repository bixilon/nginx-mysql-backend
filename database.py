import mysql.connector


def connect(databaseConfig):
    return mysql.connector.connect(
        host=databaseConfig["host"],
        user=databaseConfig["username"],
        password=databaseConfig["password"],
        db=databaseConfig["database"]
    )
