# MySQL to Nginx

This script reads a mysql database and generates the corresponding nginx config files.

It checks the database every hour (to poll the data). I recommend you, to start it as `root` (because this script reloads nginx automatically) and with systemd:

```
[Unit]
Description=nginx-mysql-backend

[Service]
User=root
Group=root
WorkingDirectory=<Path to the base folder of the script>
LimitNOFILE=4096
ExecStart=python3 nginx-mysql-backend.py <Path to config file> <Optional host name>
Type=simple
Restart=always
RestartSec=3
StartLimitInterval=600

[Install]
WantedBy=multi-user.target
```

## How to use
 1. Clone repository
 2. Create a mysql database and write the credentials into `config.json`
 3. Import the default database `databse.sql`
 4. Create the systemd service (`/etc/systemd/system/nginx-mysql-backend.service`) and start it with `systemctl enable --now nginx-mysql-backend.service`
 

## Config
Some settings are optional, take a look at `templates/config_templates.json`. 
