import glob
import json
import os
import pathlib
import signal
import socket
import sys
import traceback
from threading import Timer

import netifaces

import config
import database
import options

CONFIG = {}
DATABASE = None

TIMER = {}

PHP_FPM_CONFIG = \
    """
[${username}_${id}]
user = www-data
group = www-data
listen = ${php_fpm_socket}
listen.owner = www-data
listen.group = www-data
listen.mode = 770
chroot = ${user_home}
chdir = /
pm = ondemand
pm.max_children = 4
catch_workers_output = 1

; Huge thanks to https://www.vennedey.net/resources/3-Secure-webspaces-with-NGINX-PHP-FPM-chroots-and-Lets-Encrypt

; Flags & limits
php_value[session.save_path] = /sessions
php_admin_value[error_log] = /error.log
php_admin_flag[log_errors] = on
php_flag[display_errors] = off
php_admin_flag[expose_php] = off
php_admin_value[memory_limit] = 128M
php_admin_value[post_max_size] = 128M
php_admin_value[upload_max_filesize] = 128M
php_admin_value[cgi.fix_pathinfo] = 0
php_admin_value[disable_functions] = apache_child_terminate,apache_get_modules,apache_get_version,apache_getenv,apache_lookup_uri,apache_note,apache_request_headers,apache_reset_timeout,apache_response_headers,apache_setenv,virtual,chroot,proc_close,proc_get_status,proc_nice,chgrp,chown,filegroup,fileinode,fileowner,lchgrp,lchown,cli_get_process_title,cli_set_process_title,gc_collect_cycles,gc_disable,gc_enable,php_ini_loaded_file,php_ini_scanned_files,php_logo_guid,zend_logo_guid,zend_thread_id,define_syslog_variables,syslog,nsapi_request_headers,nsapi_response_headers,nsapi_virtual,pcntl_alarm,pcntl_errno,pcntl_exec,pcntl_fork,pcntl_get_last_error,pcntl_getpriority,pcntl_setpriority,pcntl_signal_dispatch,pcntl_signal,pcntl_sigprocmask,pcntl_sigtimedwait,pcntl_sigwaitinfo,pcntl_strerror,pcntl_wait,pcntl_waitpid,pcntl_wexitstatus,pcntl_wifexited,pcntl_wifsignaled,pcntl_wifstopped,pcntl_wstopsig,pcntl_wtermsig,posix_access,posix_ctermid,posix_errno,posix_get_last_error,posix_getcwd,posix_getegid,posix_geteuid,posix_getgid,posix_getgrgid,posix_getgrnam,posix_getgroups,posix_getlogin,posix_getpgid,posix_getpgrp,posix_getpid,posix_getppid,posix_getpwnam,posix_getpwuid,posix_getrlimit,posix_getsid,posix_getuid,posix_initgroups,posix_isatty,posix_kill,posix_mkfifo,posix_mknod,posix_setegid,posix_seteuid,posix_setgid,posix_setpgid,posix_setsid,posix_setuid,posix_strerror,posix_times,posix_ttyname,posix_uname,setproctitle,setthreadtitle,shmop_close,shmop_delete,shmop_open,shmop_read,shmop_size,shmop_write

; Session
php_admin_value[session.cookie_httponly] = on
php_admin_value[session.gc_probability] = 1
php_admin_value[session.gc_divisor] = 1000
php_admin_value[session.gc_maxlifetime] = 86400

; Pathes
php_admin_value[include_path] = .
php_admin_value[upload_tmp_dir] = /tmp/upload
php_admin_value[session.save_path] = /tmp/session
php_admin_value[sendmail_path] = /bin/sendmail -f -i
php_admin_value[openssl.capath] = /etc/ssl/certs
php_admin_value[session.entropy_file] = /dev/urandom
php_admin_value[openssl.capath] = /etc/ssl/certs
"""

CHROOT_MOUNTS = [
    "/etc/hosts",
    "/etc/resolv.conf",
    "/etc/ssl/certs",
    "/usr/share/ca-certificates",
    "/dev/null",
    "/dev/random",
    "/dev/urandom",
    "/dev/zero",
    "/var/run/nscd",
    "/usr/share/zoneinfo",
    "/lib",
    "/lib64",
    "/bin"
]

VHOST_ENABLED = False


def areWeNode(nodes):
    global CONFIG
    if len(nodes) == 0:
        return False

    if len(sys.argv) >= 3 and sys.argv[2] in nodes:
        return True

    if "hostname" in CONFIG["general"] and CONFIG["general"]["hostname"] in nodes or socket.gethostname() in nodes:
        return True

    for interfaceName in netifaces.interfaces():
        try:
            if netifaces.ifaddresses(interfaceName)[netifaces.AF_INET][0]['addr'] in nodes:
                return True
        except Exception:
            pass
    return False


def parseNginxOptions(data):
    global CONFIG
    result = []
    for key in data:
        if key == "locations":
            for location in data[key]:
                result.append(options.LocationOption(location, data[key][location]))
        elif key == "nginx-mysql-backend":
            # Reserved for us
            pass
        else:
            result.append(options.SimpleNginxOption(key, data[key]))
    return result


def parseSSLOptions(certificate):
    global CONFIG
    return [options.SSLCertificate(CONFIG["general"]["ssl"]["base_path"], CONFIG["general"]["ssl"]["cert"], certificate),
            options.SSLPrivateKey(CONFIG["general"]["ssl"]["base_path"], CONFIG["general"]["ssl"]["key"], certificate)]


def clearNginxSite():
    global CONFIG
    if os.path.isfile(CONFIG["nginx"]["site"]):
        os.remove(CONFIG["nginx"]["site"])

    for filename in glob.glob("/etc/php/%s/fpm/pool.d/nginx_mysql_*.conf" % CONFIG["general"]["php"]["version"]):
        os.remove(filename)

    for folder in glob.glob("/srv/www/*"):
        for mount in CHROOT_MOUNTS:
            if os.path.isfile(folder + mount) or os.path.isdir(folder + mount):
                os.system("umount " + folder + mount)

    print("Cleared nginx config!")


def generateNginxSite():
    global DATABASE, VHOST_ENABLED
    print("Generating a new nginx config...")

    generatedNginxConfig = open("templates/nginx").read()

    cursor = DATABASE.cursor(dictionary=True)
    cursor.execute("SELECT * FROM proxy WHERE status = 'ENABLED'")
    mysqlResult = cursor.fetchall()
    cursor.close()

    for proxyEntry in mysqlResult:
        try:
            nodes = json.loads(proxyEntry["nodes"].decode('utf-8'))

            if not areWeNode(nodes):
                continue

            proxyOptions = []
            # database settings (tables)

            generatedNginxConfig += "\n" + options.optionsToString([options.UpstreamProxy(proxyEntry["id"], proxyEntry["proxy_pass"].decode('utf-8'))], intend=False)

            serverAddress = options.ServerAddress(proxyEntry["server_address"].decode('utf-8'))
            proxyOptions.append(serverAddress)

            proxyOptions.append(options.Listen(False, True, serverAddress.defaultServer))
            proxyOptions.append(options.Listen(True, True, serverAddress.defaultServer))

            # general stuff
            extra_config = json.loads(proxyEntry["config"].decode('utf-8'))
            if "features" not in extra_config or "skip_proxy" not in extra_config["features"] or not extra_config["features"]["skip_proxy"]:
                proxyOptions.extend(parseNginxOptions(CONFIG["nginx"]["proxy"]))
            proxyOptions.extend(parseSSLOptions(proxyEntry["ssl_certificate"]))

            if "features" in extra_config:
                extra_config.pop("features")
            proxyOptions.extend(parseNginxOptions(extra_config))

            # add to output file
            generatedNginxConfig += "\n"
            generatedNginxConfig += "# Generated from proxy id %d\n" % proxyEntry["id"]
            generatedNginxConfig += "server {\n"
            generatedNginxConfig += options.optionsToString(proxyOptions).replace("${id}", str(proxyEntry["id"]))
            generatedNginxConfig += "}"
        except Exception as e:
            traceback.print_exc(e)

    cursor = DATABASE.cursor(dictionary=True)
    cursor.execute("SELECT * FROM vhost WHERE status = 'ENABLED'")
    mysqlResult = cursor.fetchall()
    cursor.close()

    for vhostEntry in mysqlResult:
        try:
            nodes = json.loads(vhostEntry["nodes"].decode('utf-8'))

            if not areWeNode(nodes):
                continue

            vhostOptions = []
            # database settings (tables)

            addresses = json.loads(vhostEntry["server_address"].decode('utf-8'))
            addresses.append(vhostEntry["domain"])

            serverAddress = options.ServerAddress(addresses)
            vhostOptions.append(serverAddress)

            vhostOptions.append(options.Listen(False, False, serverAddress.defaultServer))
            vhostOptions.append(options.Listen(True, False, serverAddress.defaultServer))

            vhostCustomConfig = json.loads(vhostEntry["config"].decode('utf-8'))

            features = {
                "add_php_location": True,
                "add_php_fpm_file": True,
                "add_root": True,
                "add_root_try_files": True
            }

            if "nginx-mysql-backend" in vhostCustomConfig:
                for feature in features:
                    if feature in vhostCustomConfig["nginx-mysql-backend"]:
                        features[feature] = vhostCustomConfig["nginx-mysql-backend"][feature]

            if features["add_root"]:
                vhostOptions.append(options.SimpleNginxOption("root", "/srv/www/${username}/${domain}/html"))

            if features["add_root_try_files"]:
                vhostOptions.append(options.LocationOption("/", [
                    {
                        "attribute": "try_files",
                        "value": "$uri $uri/ =404"
                    }
                ]))

            if features["add_php_location"]:
                vhostOptions.append(options.LocationOption("~ \\.php$", [
                    {
                        "attribute": "try_files",
                        "value": "$uri =404"
                    },
                    {
                        "attribute": "fastcgi_split_path_info",
                        "value": r"^(.+\.php)(/.+)$"
                    },
                    {
                        "attribute": "fastcgi_pass",
                        "value": "unix:${php_fpm_socket}"
                    },
                    {
                        "attribute": "fastcgi_param",
                        "value": "SCRIPT_FILENAME ${domain}/html/$fastcgi_script_name"
                    },
                    {
                        "attribute": "fastcgi_index",
                        "value": "index.php"
                    },
                    {
                        "attribute": "include",
                        "value": "/etc/nginx/fastcgi_params"
                    }
                ]))

            # general stuff
            vhostOptions.extend(parseNginxOptions(CONFIG["nginx"]["common"]))
            vhostOptions.extend(parseNginxOptions(CONFIG["nginx"]["vhost"]))

            vhostOptions.extend(parseNginxOptions(vhostCustomConfig))

            # add to output file
            currentConfig = "\n"
            currentConfig += "# Generated from vhost id %d\n" % vhostEntry["id"]
            currentConfig += "server {\n"
            currentConfig += options.optionsToString(vhostOptions)
            currentConfig += "}"

            # generate fpm file
            chroot = "/srv/www/%s" % vhostEntry["username"]
            fpm = PHP_FPM_CONFIG

            VARIABLES = {
                "${user_home}": chroot,
                "${username}": vhostEntry["username"],
                "${domain}": vhostEntry["domain"],
                "${id}": vhostEntry["id"],
                "${php_fpm_socket}": "/run/php/%d_%s.sock" % (vhostEntry["id"], vhostEntry["username"])
            }

            # replace all variables
            for variable in VARIABLES:
                fpm = fpm.replace(variable, str(VARIABLES[variable]))
                currentConfig = currentConfig.replace(variable, str(VARIABLES[variable]))

            if features["add_php_fpm_file"]:
                fpmFile = open("/etc/php/%s/fpm/pool.d/nginx_mysql_%s_%s.conf" % (CONFIG["general"]["php"]["version"], vhostEntry["username"], str(vhostEntry["id"])), "w")
                fpmFile.write(fpm)
                fpmFile.close()

            generatedNginxConfig += currentConfig

            # create temp folder
            pathlib.Path(chroot + "/tmp/session/").mkdir(parents=True, exist_ok=True)

            for mount in CHROOT_MOUNTS:
                path = pathlib.Path(chroot + mount)
                if path.is_mount():
                    print("Skipping " + chroot + mount)
                    continue
                if not os.path.isdir(mount):
                    path.parent.mkdir(parents=True, exist_ok=True)

                    if not path.exists():
                        with open(chroot + mount, 'w'):
                            pass

                    os.system("mount --bind -o ro %s %s" % (mount, chroot + mount))
                else:
                    path.mkdir(parents=True, exist_ok=True)
                    os.system("mount --bind -o ro %s %s" % (mount, chroot + mount))

            VHOST_ENABLED = True

        except Exception as e:
            traceback.print_exc(e)

    file = open(CONFIG["nginx"]["site"], "w")
    file.write(generatedNginxConfig)
    file.close()

    print("Generated and dumped nginx config!")


def cron():
    global CONFIG
    clearNginxSite()
    generateNginxSite()

    if VHOST_ENABLED:
        os.system("systemctl restart php%s-fpm.service" % CONFIG["general"]["php"]["version"])

    os.system("systemctl reload nginx")


def main():
    global CONFIG, DATABASE, TIMER
    print("Nginx MySQL Backend")

    print("Loading config...")
    CONFIG = config.loadConfig()

    print("Connecting to database...")

    DATABASE = database.connect(CONFIG["database"])

    cron()
    TIMER = PerpetualTimer(CONFIG["general"]["poll_delay"], cron)
    TIMER.start()


class PerpetualTimer:
    def __init__(self, delay, executor):
        self.delay = delay
        self.executorFunction = executor
        self.timer = Timer(self.delay, self.executor)

    def executor(self):
        self.executorFunction()
        self.timer = Timer(self.delay, self.executor)
        self.timer.start()

    def start(self):
        self.timer.start()

    def cancel(self):
        self.timer.cancel()


if __name__ == '__main__':
    main()

signal.signal(signal.SIGTERM, clearNginxSite)
