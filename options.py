import json
from abc import abstractmethod


def toStringList(data):
    if type(data) == list or data.startswith("["):
        # json
        output = ""
        if type(data) == list:
            dataList = data
        else:
            dataList = json.loads(data)

        for value in dataList:
            output += value + " "
        if output.endswith(' '):
            output = output[:-1]
        return output
    else:
        # string
        return data


def optionsToString(optionList, intend=True):
    current = ""
    for option in optionList:
        if intend:
            current += "    "
        current += "%s " % option.getNginxAttributeName()
        value = option.getNginxAttributeValue()

        if "\n" in value:
            lines = option.getNginxAttributeValue().split("\n")

            firstLine = True
            for line in lines:
                if firstLine:
                    current += line + "\n"
                    firstLine = False
                else:
                    if intend:
                        current += "    "
                    current += "%s\n" % line
            if current.endswith("\n"):
                current = current[:-1]
        else:
            current += value

        if option.needsSemicolon():
            current += ";"
        current += "\n"

    return current


class NginxOption:
    def __init__(self, data):
        self.data = data

    @abstractmethod
    def getNginxAttributeName(self): raise NotImplementedError

    @abstractmethod
    def getNginxAttributeValue(self): raise NotImplementedError

    def needsSemicolon(self):
        return True


class ServerAddress(NginxOption):
    def __init__(self, data):
        super().__init__(data)
        self.output = toStringList(data)

        self.defaultServer = self.output == "" or "_" in self.output.split(" ")

    def getNginxAttributeName(self):
        return "server_name"

    def getNginxAttributeValue(self):
        return self.output

    def isDefaultServer(self):
        return self.defaultServer


class Listen(NginxOption):
    def __init__(self, ipv6, ssl, defaultServer):
        super().__init__({})
        self.ipv6 = ipv6
        self.ssl = ssl
        self.defaultServer = defaultServer

    def getNginxAttributeName(self):
        return "listen"

    def getNginxAttributeValue(self):
        base = ""
        if self.ssl:
            base += "443 ssl http2"
        else:
            base += "80"
        if self.ipv6:
            base = "[::]:" + base

        if self.defaultServer:
            return base + " default_server"
        else:
            return base


class SSLCertificateOption(NginxOption):
    def __init__(self, defaultPath, certificatePathname, certificate):
        super().__init__({})
        self.defaultPath = defaultPath
        self.certSuffix = certificatePathname
        self.certificate = certificate

    @abstractmethod
    def getNginxAttributeName(self):
        raise NotImplementedError

    def getNginxAttributeValue(self):
        return "\"%s/%s/%s\"" % (self.defaultPath, self.certificate, self.certSuffix)


class SSLCertificate(SSLCertificateOption):
    def __init__(self, defaultPath, certificatePathname, certificate):
        super().__init__(defaultPath, certificatePathname, certificate)

    def getNginxAttributeName(self):
        return "ssl_certificate"


class SSLPrivateKey(SSLCertificateOption):
    def __init__(self, defaultPath, certificatePathname, certificate):
        super().__init__(defaultPath, certificatePathname, certificate)

    def getNginxAttributeName(self):
        return "ssl_certificate_key"


class LocationOption(NginxOption):
    def __init__(self, location, data):
        if type(data) == list:
            super().__init__(data)
        else:
            super().__init__(json.loads(data))

        self.location = location

    def getNginxAttributeName(self):
        return "location"

    def getNginxAttributeValue(self):
        output = "%s {\n" % self.location

        options = []
        for optionValue in self.data:
            options.append(SimpleNginxOption(optionValue["attribute"], optionValue["value"]))

        output += optionsToString(options)

        output += "}"

        return output

    def needsSemicolon(self):
        return False


class SimpleNginxOption(NginxOption):
    def __init__(self, key, value):
        super().__init__(value)
        self.key = key

    def getNginxAttributeName(self):
        return self.key

    def getNginxAttributeValue(self):
        return self.data


class UpstreamProxy(NginxOption):
    def __init__(self, id, data):
        if type(data) == list:
            super().__init__(data)
        else:
            super().__init__(json.loads(data))
        self.id = id

    def getNginxAttributeName(self):
        return "upstream"

    def getNginxAttributeValue(self):
        output = "proxy%s {\n" % self.id

        values = []
        for optionValue in self.data:
            values.append(SimpleNginxOption("server", optionValue))

        output += optionsToString(values)

        output += "}"

        return output

    def needsSemicolon(self):
        return False
